# Description
Site that will cycle through trips that you have made similar to the funcitonality of an electronic picture frame. By clicking on the page it will bring up details about the trip.

### Instructions
This utilizes [Google Maps Directions API](https://developers.google.com/maps/documentation/directions/). From that link, you will need to get your own API key, which may involve creating a new project. Once you have an API key, insert into apiKey.js.

To enter your own trips, you should follow the format of trips.js. Each trip can contain the following attributes:

    title: string,
    origin: string,
    destination: string,
    waypoints: [
        { location: string, stopover: bool },
    ],
    startDate: string,
    endDate: string,
    details: string,
    travelMode: string,
    photoAlbum: imgurEmbedAlbum (looks like this: <blockquote class="imgur-embed-pub" lang="en" data-id="a/Hna2m"><a href="//imgur.com/Hna2m"></a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>)

#### Notes
Standard API limits for Google Maps Directions API:

* 2,500 free directions requests per day, calculated as the sum of client-side and server-side queries.
* Up to 23 waypoints allowed in each request, whether client-side or server-side queries.
* 50 requests per second, calculated as the sum of client-side and server-side queries.

Because of this, the current interval is set to 105 seconds per API call.

Currently, Google Maps Directions API only allows for the following travel modes:
* DRIVING
* WALKING
* BICYCLING
* TRANSIT

#### Future
* Hopefully flights get added to the API.
  * [Click here and click the star to move this feature request up the list](https://issuetracker.google.com/issues?q=componentid:188841%2B%20status:(open%20%7C%20new%20%7C%20assigned%20%7C%20accepted)%20flight). Maybe make a comment too.

#### Demo
Note: This demo has the timing sped up to show the transition, it is set to 105 seconds by default

![Travel Slideshow Demo](TravelSlideshow_V1_0.mov)