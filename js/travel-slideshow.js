var paused = false;
var roadtripIndex = 0;
var roadTripHasChanged = true;

var roadtripCount = roadtrips.length;

function initMap() {
    document.getElementById('details').style.visibility = "hidden";
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 7,
        center: { lat: 41.85, lng: -87.65 }
    });
    directionsDisplay.setMap(map);

    var onChangeHandler = function() {
        calculateAndDisplayRoute(directionsService, directionsDisplay);
    };
    calculateAndDisplayRoute(directionsService, directionsDisplay);

    setInterval(function() { calculateAndDisplayRoute(directionsService, directionsDisplay) }, 105000);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    if (paused) {
        return;
    }
    roadTripHasChanged = true;
    roadtripIndex += 1;
    if (roadtripIndex >= roadtripCount) { roadtripIndex = 0; }

    directionsService.route({
        origin: roadtrips[roadtripIndex]['origin'],
        destination: roadtrips[roadtripIndex]['destination'],
        waypoints: roadtrips[roadtripIndex]['waypoints'],
        travelMode: roadtrips[roadtripIndex]['travelMode'],
    }, function(response, status) {
        if (status === 'OK') {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

function showDetails() {
    paused = !paused;

    if (paused) {
        roadTrip = roadtrips[roadtripIndex];
        console.log(roadTrip);
        populateTag('TripName', roadTrip["title"]);
        var tripDates = roadTrip["startDate"] && roadTrip["endDate"] ? roadTrip["startDate"] + " - " + roadTrip["endDate"] : roadTrip["startDate"] != "" ? roadTrip["startDate"] : "";
        populateTag("TripDates", tripDates);
        populateTag("TripDetails", roadTrip["details"]);
        if (roadTripHasChanged) {
            if (roadTrip["photoAlbum"]) {
                $("#PhotoAlbum").children().remove();
                $('#PhotoAlbum').append(roadTrip["photoAlbum"]);
            }
        }
        document.getElementById('details').style.visibility = "";
        roadTripHasChanged = false;
    } else {
        document.getElementById('details').style.visibility = "hidden";
    }
}

function populateTag(tagId, text) {
    var htmlTag = document.getElementById(tagId);
    if (text) {
        htmlTag.style.visibility = "";
        htmlTag.innerText = text;
    } else {
        htmlTag.style.visibility = "hidden";
    }
}