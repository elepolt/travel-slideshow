var roadtrips = [{
        title: 'Moving to Denver',
        origin: 'cincinnati, oh',
        destination: 'denver, co',
        waypoints: [
            { location: 'chicago, il', stopover: true },
            { location: 'badlands National park', stopover: true },
            { location: 'mount rushmore', stopover: true },
            { location: "devil's tower national monument", stopover: true },
        ],
        startDate: "April 27, 2014",
        endDate: "May 2, 2014",
        details: "Moved from Cincinnati to Denver with mom. Stopped and visted Betsy and Adam in Chicago. Saw the Bad Lands, Mount Rushmore, and Devil's Tower",
        travelMode: "DRIVING"
    },
    {
        title: 'Ski Trip 07',
        origin: 'perrysburg, oh',
        destination: 'Boyne City, mi',
        waypoints: [],
        startDate: "",
        endDate: "",
        details: "",
        travelMode: "DRIVING"
    },
]